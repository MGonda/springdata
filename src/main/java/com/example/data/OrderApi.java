package com.example.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/Order")
public class OrderApi {
    private OrderManager orders;

    @Autowired
    public OrderApi(OrderManager orders) {
        this.orders = orders;
    }

    @GetMapping("/api/order/all")
    public Iterable<Order> getAll() {
        return  orders.getAll();
    }
    @GetMapping("/api/order")
    public Optional<Order> getByID(@RequestParam Long index){

        return orders.getID(index);

    }

    @PostMapping("/api/order")
    public Order addOrder(@RequestBody Order order){
        return orders.save(order);
    }

    @PutMapping("/api/admin/order")
    public Order updateOrder(@RequestBody Order order){
        return orders.save(order);
    }

    @PatchMapping("/api/admin/order")
    public Order partupdateCustomer(@RequestBody Order order ) {
        return orders.save(order);
    }
}
