package com.example.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/Customer")
public class CustomerApi {
    private CustomerManager customers;

    @Autowired
    public CustomerApi(CustomerManager customers) {
        this.customers = customers;
    }

    @GetMapping("/api/customer/all")
    public Iterable<Customer> getAll() {
        return  customers.getAll();
    }

    @GetMapping("/api/customer")
    public Optional<Customer> FindByID(@RequestParam Long index){

        return customers.getID(index);
    }

    @PostMapping("/api/admin/customer")
    public Customer addCustomer(@RequestBody Customer customer){
        return customers.save(customer);
    }

    @PutMapping("/api/admin/customer")
    public Customer updateCustomer(@RequestBody Customer customer){
        return customers.save(customer);
    }

    @PatchMapping("/api/admin/customer")
    public Customer partupdateCustomer(@RequestBody Customer customer ){
        return customers.save(customer);
    }


}
