package com.example.data;

import jdk.nashorn.internal.runtime.options.Option;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/Product")
public class ProductApi {

    private ProductManager products;

    public ProductApi(ProductManager products) {
        this.products = products;
    }

    @GetMapping("/api/product/all")
    public Iterable<Product> getAll() {
        return  products.getAll();
    }

    @GetMapping("/api/product")
    public Optional<Product> getByID(@RequestParam Long index){

        return products.getID(index);

    }
    @PostMapping("/api/admin/product")
    public Product addEQ(@RequestBody Product product){
        return products.save(product);
    }

    @PutMapping("/api/admin/product")
    public Product updateEQ(@RequestBody Product product){
        return products.save(product);
    }
    @PatchMapping("/api/admin/product")
    public Product partupdateCustomer(@RequestBody Product product ) {
        return products.save(product);
    }
}
