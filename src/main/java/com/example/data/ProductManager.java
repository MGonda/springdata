package com.example.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
public class ProductManager {
    private ProductRepo productRepo;

    @Autowired
    public ProductManager(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    public Optional<Product> getID(Long id) {
        return productRepo.findById(id);
    }

    public Iterable<Product> getAll() {
        return productRepo.findAll();
    }

    public Product save(Product product) {
        return productRepo.save(product);
    }

    public Product partupdateProduct(Map<String, Object> updates, Long id) {
        Optional<Product> productOptional = productRepo.findById(id);
        if (!productOptional.isPresent())
            return null;
        Product product = productOptional.get();
        if (updates.containsKey("name")) product.setName((String) updates.get("name"));
        if (updates.containsKey("price")) product.setPrice((Float) updates.get("price"));
        if (updates.containsKey("avaiable")) product.setAvaiable((Boolean) updates.get("avaiable"));
        return productRepo.save(product);

    }
}
