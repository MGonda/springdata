package com.example.data;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {
    private ProductRepo productRepository;
    private OrderRepo orderRepository;
    private CustomerRepo customerRepository;

    @Autowired
    public DbMockData(ProductRepo productRepository, OrderRepo orderRepository, CustomerRepo customerRepository) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
    }



    @EventListener(ApplicationReadyEvent.class)
    public void fill() {
        Product product = new Product("Jaja",3.1F, true);
        Product product2 = new Product("Mąka",3.4F, false);
        Product product3 = new Product("Rzepa",2.5F, false);
        Product product4 = new Product("Cukier",1.7F, true);
        Customer customer = new Customer("Maciej","Wrocław");
        Set<Product> products = new HashSet<Product>() {
            {
                add(product);
                add(product2);
                add(product3);
                add(product4);
            }};
        Order order = new Order(customer, products, LocalDateTime.now(), "in progress");

        productRepository.save(product);
        productRepository.save(product2);
        productRepository.save(product3);
        productRepository.save(product4);
        customerRepository.save(customer);
        orderRepository.save(order);
    }
}