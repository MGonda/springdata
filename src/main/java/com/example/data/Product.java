package com.example.data;

import com.sun.javafx.beans.IDProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    private String name;
    private float price;
    private boolean avaiable;

    public Product() {
    }

    public Product(String name, float price, boolean avaiable) {
        this.name = name;
        this.price = price;
        this.avaiable = avaiable;
    }

    public Product(Long id, String name, float price, boolean avaiable) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.avaiable = avaiable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isAvaiable() {
        return avaiable;
    }

    public void setAvaiable(boolean avaiable) {
        this.avaiable = avaiable;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", avaiable=" + avaiable +
                '}';
    }
}
