package com.example.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class CustomerManager {

    private CustomerRepo customerRepo;

    @Autowired
    public CustomerManager(CustomerRepo customerRepo){
        this.customerRepo = customerRepo;
    }

    public Optional<Customer> getID(Long id){
        return customerRepo.findById(id);
    }

    public Iterable<Customer> getAll() {
        return customerRepo.findAll();
    }

    public Customer save(Customer customer){
        return customerRepo.save(customer);
    }
    public Customer partupdateCustomer(Map<String, Object> updates,long id){
        Optional<Customer> customerOptional = customerRepo.findById(id);
        if(!customerOptional.isPresent())
            return null;
        Customer customer = customerOptional.get();
        if(updates.containsKey("name")) customer.setName((String)updates.get("name"));
        if(updates.containsKey("price")) customer.setName((String)updates.get("adress"));
        return customerRepo.save(customer);
    }



}
