package com.example.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
public class OrderManager {
    private OrderRepo orderRepo;

    @Autowired
    public OrderManager(OrderRepo orderRepo) {
        this.orderRepo = orderRepo;
    }

    public Optional<Order> getID(Long id) {
        return orderRepo.findById(id);
    }

    public Iterable<Order> getAll() {
        return orderRepo.findAll();
    }

    public Order save(Order order) {
        return orderRepo.save(order);

    }

    public Order partupdateOrder(Map<String, Object> updates, Long id) {
        Optional<Order> orderOptional = orderRepo.findById(id);
        if (!orderOptional.isPresent())
            return null;
        Order order = orderOptional.get();
        if (updates.containsKey("customer")) order.setCustomer((Customer) updates.get("customer"));
        if (updates.containsKey("products")) order.setProducts((Set<Product>) updates.get("products"));
        if (updates.containsKey("placeDate")) order.setPlaceDate((LocalDateTime) updates.get("placeDate"));
        if (updates.containsKey("status")) order.setStatus((String) updates.get("status"));
        return orderRepo.save(order);


    }
}
