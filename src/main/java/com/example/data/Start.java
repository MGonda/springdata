package com.example.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class Start {


    private ProductRepo productRepo;
    private CustomerRepo customerRepo;

    public Start(ProductRepo productRepo, CustomerRepo customerRepo) {
        this.productRepo = productRepo;
        this.customerRepo = customerRepo;
    }

    @Autowired

    @EventListener(ApplicationReadyEvent.class)
    public void runExample(){
        Product product = new Product("Mleko",3.1F, true);
        Product product2 = new Product("Kawa",3.4F, false);
        Product product3 = new Product("Herbata",2.5F, false);
        Product product4 = new Product("Woda",1.7F, true);
        Customer customer = new Customer("Maciej","Wrocław");

        productRepo.save(product);
        productRepo.save(product2);
        productRepo.save(product3);
        productRepo.save(product4);
        customerRepo.save(customer);
    }

}
